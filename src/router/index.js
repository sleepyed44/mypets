import { createRouter, createWebHistory } from "vue-router"; // cài vue-router: npm install vue-router@next --save

const routes = [
    {
        path : '/',
        component: ()=>import('../layout/wrapper/index.vue')
    },
    {
        path : '/admin/the-gioi-pets',
        component: ()=>import('../components/Thegioipets/index.vue')
    },
    {
        path : '/admin/spa',
        component: ()=>import('../components/Spa/index.vue')
    },{
        path : '/admin/phukien',
        component: ()=>import('../components/PhuKien/index.vue')
    },
]

const router = createRouter({
    history: createWebHistory(),
    routes: routes
})

export default router